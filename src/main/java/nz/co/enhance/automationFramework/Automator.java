package nz.co.enhance.automationFramework;

import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.windows.WindowsDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import nz.co.enhance.automationFramework.HelperClasses.FullPageScreenshot;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Automator {
    public static WebDriver driver;
    public static RemoteWebDriver winAppDriver; //This is separate from the webdriver to allow both to be accessed at once

    public String appiumURL = "http://127.0.0.1:4723/wd/hub";
    public String windowsDriverURL = "http://127.0.0.1:4723/";

    public boolean maximiseBrowsers = true;  // Default setting to maximise Firefox, Chrome and Safari, can be toggled false
    public boolean takeFullPageScreenshots = false; //set to true to debugging purposes

    //default constructor
    public Automator() {
    }

    public void startChrome(ChromeOptions chromeOptions) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver(chromeOptions);
        maximiseBrowserIfRequired();
    }

    public void startChrome() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver();
        maximiseBrowserIfRequired();
    }

    public void startFirefox(FirefoxOptions firefoxOptions) {
        WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
        driver = new FirefoxDriver(firefoxOptions);
        maximiseBrowserIfRequired();
    }

    public void startFirefox() {
        WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
        driver = new FirefoxDriver();
        maximiseBrowserIfRequired();
    }

    public void startSafari(SafariOptions safariOptions) {
        WebDriverManager.getInstance(DriverManagerType.SAFARI).setup();
        driver = new SafariDriver(safariOptions);
        maximiseBrowserIfRequired();
    }

    public void startSafari() {
        WebDriverManager.getInstance(DriverManagerType.SAFARI).setup();
        driver = new SafariDriver();
        maximiseBrowserIfRequired();
    }

    public void startEdge(EdgeOptions edgeOptions) {
        WebDriverManager.getInstance(DriverManagerType.EDGE).setup();
        driver = new EdgeDriver(edgeOptions);
        maximiseBrowserIfRequired();
    }

    public void startEdge() {
        WebDriverManager.getInstance(DriverManagerType.EDGE).setup();
        driver = new EdgeDriver();
        maximiseBrowserIfRequired();
    }

    public void startIE(InternetExplorerOptions internetExplorerOptions) {
        WebDriverManager.getInstance(DriverManagerType.IEXPLORER).setup();
        driver = new InternetExplorerDriver(internetExplorerOptions);
        maximiseBrowserIfRequired();
    }

    public void startIE() {
        WebDriverManager.getInstance(DriverManagerType.IEXPLORER).setup();
        InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
        // some standard required options to make IE work smoothly
        internetExplorerOptions.introduceFlakinessByIgnoringSecurityDomains();
        internetExplorerOptions.requireWindowFocus();
        driver = new InternetExplorerDriver(internetExplorerOptions);
        maximiseBrowserIfRequired();
    }

    //used the default URL as specfied in this Automator
    public void startWinAppDriver(DesiredCapabilities caps) {
        startWinAppDriver(caps, this.windowsDriverURL);

    }

    //used a custom URL
    public void startWinAppDriver(DesiredCapabilities caps, String URL) {
        try {
            driver = new WindowsDriver(new URL(URL), caps);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    //used the default Appium URL as specfied in this Automator
    public void startRemoteWebDriver(DesiredCapabilities caps) {
        startRemoteWebDriver(caps, this.appiumURL);
    }

    //used a custom URL
    public void startRemoteWebDriver(DesiredCapabilities caps, String URL) {
        if (caps.getCapability("platformName").toString().equalsIgnoreCase("android")) {
            try {
                driver = new AndroidDriver(new URL(URL), caps);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else if (caps.getCapability("platformName").toString().equalsIgnoreCase("ios")) {
            try {
                driver = new IOSDriver(new URL(URL), caps);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            try { // Other remote drivers such as using Browserstack/CBT/SauceLabs/AWS
                driver = new RemoteWebDriver(new URL(URL), caps);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

    }

    public final double getWidth() {
        return driver.manage().window().getSize().getWidth();
    }

    public final double getHeight() {
        return driver.manage().window().getSize().getHeight();
    }

    public final int getHeightAsInt() {
        return driver.manage().window().getSize().getHeight();
    }


    public void quit() {
        driver.quit();
    }


    //region Browser Based commands
    //---------------------------------------------------------------------------------------

    public void takeScreenshot(Scenario scenario) {
        if (takeFullPageScreenshots) {
            takeFullPageScreenshot(scenario);
        } else {
            takeNormalScreenshot(scenario);
        }
    }

    private void takeNormalScreenshot(Scenario scenario) {
        byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
    }

    //FA 8/4/19 full page screenshotting solution for debuggng purposes
    private void takeFullPageScreenshot(Scenario scenario) {
        FullPageScreenshot fullPageScreenshot = new FullPageScreenshot();
        fullPageScreenshot.capturePageScreenshot(driver, scenario);
    }

    public void deleteAllCookies() {
        driver.manage().deleteAllCookies();
    }

    public void maximiseBrowserIfRequired() {
        if (maximiseBrowsers) {
            maximiseBrowser();
        }
    }

    public void maximiseBrowser() {
        driver.manage().window().maximize();

    }

    public void navigateTo(String URL) {
        driver.navigate().to(URL);
    }

    public void switchToDefaultTab() {
        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(0));
    }

    public void switchToTab(int index) {
        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(index));
    }

    public void closeTab(int index) {
        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(index));
        driver.close();
        switchToDefaultTab();
    }

    public String getCurrentPageUrl() {
        return driver.getCurrentUrl();
    }

    public void scrollToBottomOfPage() {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void scrollToTopOfPage() {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, 0)");
    }

    public void scrollTo(WebElement element) {
        Point hoverItem = element.getLocation();
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + (hoverItem.getY() - 400) + ");");
    }

    public void switchToDefaultFrame() {
        driver.switchTo().defaultContent();
    }

    public void switchToFrame(By by) {
        try {
            driver.switchTo().frame(driver.findElement(by));
        } catch (Exception e) {
            //already on it;
        }
    }


    //endregion

}
