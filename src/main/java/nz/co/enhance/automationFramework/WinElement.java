package nz.co.enhance.automationFramework;

import io.appium.java_client.windows.WindowsDriver;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;


public class WinElement {
    public LocatorType locatorType = null;
    public String locator = null;
    public WebElement webElement = null;

    //when instantiating the element we DON'T fetch it. This is so we can set them up without them being visible yet.
    public WinElement(LocatorType locatorType, String locator) {
        this.locatorType = locatorType;
        this.locator = locator;
    }

    public WinElement(WebElement element) {
        webElement = element;
    }

    //Waits x seconds for the element to exist - note does not check visibility
    public void waitForElementToExist(int seconds) {
        int i = 0;
        while (i < seconds * 20) {
            if (exists()) {
                return;
            } else {
                HelperMethods.sleep((long) 50);
                i++;
            }
        }
        try {
            throw new Exception("Element does not exist after " + seconds + " seconds.");
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(false);
        }
    }

    public WebElement returnElementWhenExists(int seconds) {
        waitForElementToExist(seconds);
        return webElement;
    }

    public WebElement returnElementWhenDisplayed(int seconds) {
        waitForElementToBeDisplayed(seconds);
        return webElement;
    }

    //Waits x seconds for the element to be displayed
    public void waitForElementToBeDisplayed(int seconds) {
        int i = 0;

        if (this.locator == null) { //if we've made this with an element then we check if it's displayed
            if (webElement.isDisplayed()) {
                return;
            }
        } else {
            while (i < (seconds * 20)) {
                if (isElementDisplayed()) {
                    return;
                } else {
                    HelperMethods.sleep((long) 50);
                    i++;
                }
            }
            //we only get to here if the element is not displayed
            try {
                throw new AssertionError("Element is not displayed after " + seconds + " seconds.");
            } catch (Exception e) {
                e.printStackTrace();
                assert (false);

            }
        }
    }

    //Waits x seconds for the element's attribute to be the specified value
    public void waitForElementAttribute(int seconds, String attribute, String value) {
        int i = 0;
        while (i < seconds) {
            if (isElementDisplayed()) {
                if (findElement().getAttribute(attribute).toLowerCase().equals(value.toLowerCase()))
                    return;
            } else {
                HelperMethods.sleep(1);
                i++;
            }
        }
        try {
            throw new AssertionError("Element's attribute " + attribute + " was not " + value + " after " + seconds + " seconds.");
        } catch (Exception e) {
            e.printStackTrace();
            assert (false);

        }
    }

    //Checks for existence
    public boolean exists() {
        if (this.locator == null) {
            return webElement.isEnabled();
        } else {
            try {
                webElement = findElementByLocator();
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }

    //Checks for visibility
    public boolean isElementDisplayed() {
        if (this.locator == null) {
            return webElement.isDisplayed();
        } else {
            try {
                webElement = findElementByLocator();
                return webElement.isDisplayed();
            } catch (Exception e) {
                return false;
            }
        }
    }


    //retrieves the webelement if it exists, otherwise return null
    public WebElement findElement() {
        if (locator == null) {
            return webElement;
        } else {
            //stale elements are being thrown sometimes due to refresh and actions on complex webpages
            for (int i = 0; i < 10; i++) {
                try {
                    return findElementByLocator();
                } catch (StaleElementReferenceException s) {
                    HelperMethods.sleep(1);
                } catch (Exception b) {
                    System.out.println("Exception: " + b);
                    i = 10;
                }
            }

            //if we get to here, it couldn't be found
            try {
                throw new Exception("Element does not exist.");
            } catch (Exception e) {
                System.out.println("Element existance check returned false");
            }

            return null;
        }

    }


    public WebElement findElementByLocator() {
        switch (this.locatorType) {
            case ID:
                return ((WindowsDriver) Automator.winAppDriver).findElementById(this.locator);
            case XPATH:
                return ((WindowsDriver) Automator.winAppDriver).findElementByXPath(this.locator);
            case ACCESSIBILITYID:
                return ((WindowsDriver) Automator.winAppDriver).findElementByAccessibilityId(this.locator);
            case NAME:
                return ((WindowsDriver) Automator.winAppDriver).findElementByName(this.locator);
            case CLASSNAME:
                return ((WindowsDriver) Automator.winAppDriver).findElementByClassName(this.locator);
            case CSSSELECTOR:
                return ((WindowsDriver) Automator.winAppDriver).findElementByCssSelector(this.locator);
            case UIAUTOMATION:
                return ((WindowsDriver) Automator.winAppDriver).findElementByWindowsUIAutomation(this.locator);
            case TAGNAME:
                return ((WindowsDriver) Automator.winAppDriver).findElementByTagName(this.locator);
            default:
                return null;
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                          Basic commands                                                        //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void click() {
        for (int i = 0; i < 10; i++) {
            try {
                if (findElement().isDisplayed()) {
                    findElement().click();
                    i = 10;
                } else if (exists()) {//if it exists but web page says it's invisible
                    forceClick();
                    i = 10;
                }
            } catch (StaleElementReferenceException s) {
                HelperMethods.sleep(1);
            }
        }
    }

    public void fastClick() {
        findElement().click();
    }

    public String getText() {
        try {
            return findElement().getText();
        } catch (StaleElementReferenceException s) {

        }
        return "";
    }

    public void sendKeys(String keys) {
        findElement().sendKeys(keys);
    }

    public void sendKeys(CharSequence character) {
        findElement().sendKeys(character);
    }

    public Boolean isDisplayed() {
        try {
            return findElement().isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public Boolean isEnabled() {
        try {
            return findElement().isEnabled();
        } catch (Exception e) {
            return false;
        }
    }

    public Boolean isSelected() {
        try {
            return findElement().isSelected();
        } catch (Exception e) {
            return false;
        }
    }

    public void clear() {
        findElement().clear();
    }

    public String getAttribute(String attributeName) {
        return findElement().getAttribute(attributeName);
    }

    public Point getLocation() {
        return findElement().getLocation();
    }

    public List<WinElement> findChildren(By by) {
        List<WinElement> elementList = new ArrayList<>();
        for (WebElement element : findElement().findElements(by)) {
            elementList.add(new WinElement(element));
        }
        return elementList;
    }

    public WinElement findChild(By by) {
        return new WinElement(findElement().findElement(by));
    }

    public WinElement getParent() {
        return new WinElement(findElement().findElement(By.xpath("..")));
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            Swipes/Scrolls/Touches                                                  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void hoverOver() {
        Robot robot;
        try {
            robot = new Robot();
            robot.mouseMove(webElement.getLocation().getX(), webElement.getLocation().getY());
        } catch (Exception e) {

        }
    }

    public void forceClick() {
        Robot robot;
        try {
            robot = new Robot();
            robot.mouseMove(webElement.getLocation().getX() + 10, webElement.getLocation().getY() + 10);
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);

        } catch (Exception e) {

        }
    }

}
