package nz.co.enhance.automationFramework.HelperClasses;

import java.util.ArrayList;
import java.util.List;

public class TestResults {

    public boolean overallResult = true;
    List<String> resultMessages = new ArrayList<>();

    public String printResults() {
        String output = "\nResults table: \n";

        for (String resultsMessage : resultMessages) {
            output += resultsMessage + "\n";
        }
        return output;
    }

    public void buildResult(String objectName, String expected, String actual) {
        if (expected.equals(actual)) {
            //passed
            resultMessages.add("PASSED: The item " + objectName + " was expected to be " + expected + " and was " + actual);
        } else {
            overallResult = false;
            resultMessages.add("FAILED: The item " + objectName + " was expected to be " + expected + " but was " + actual);
        }
    }
}
