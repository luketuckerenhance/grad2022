package nz.co.enhance.automationFramework.HelperClasses;

import cucumber.api.Scenario;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class ResultHelper {

    public static TestResult createAssertEqualsResult(int expected, int actual) {
        return createAssertEqualsResult(String.valueOf(expected), String.valueOf(actual));
    }

    public static TestResult createAssertEqualsResult(String expectedName, int expected, String actualName, int actual) {
        return createAssertEqualsResult(expectedName, String.valueOf(expected), actualName, String.valueOf(actual));
    }


    public static void assertAllResults(List<TestResult> results, Scenario scenario) {
        boolean allPassed = true;
        System.out.println("Results of comparisons: \n");
        for (TestResult result : results) {
            //add on passed or failed to the message if not already there
            if (!(result.message.toLowerCase().contains("failed")) || !(result.message.toLowerCase().contains("passed"))) {
                if (result.result == false) {
                    result.message = "FAILED:  " + result.message;
                } else {
                    result.message = "PASSED: " + result.message;
                }
            }


            System.out.println(result.message);
            scenario.write(result.message + "\n");
            allPassed = allPassed && result.result;
        }
        assertTrue(allPassed);
    }


    public static void assertAllResultsInMap(List<Map<String, String>> results, Scenario scenario) {
        boolean allPassed = true;
        String messages = "Results of comparisons: \n";
        for (Map<String, String> result : results) {
            if (result.get("result").equalsIgnoreCase("false")) {
                allPassed = false;
                messages += result.get("message") + "\n";
            }
        }
        System.out.println(messages);
        scenario.write(messages);
        assertTrue(allPassed);
    }

    public static TestResult createAssertEqualsResult(String expected, String actual) {
        TestResult result = new TestResult();
        result.actualValue = actual;
        result.expectedValue = expected;

        result.compareValues();
        if (result.result) {
            result.message = "PASSED: Expected value " + result.expectedValue + " matches actual value  " + result.actualValue + "\n";
        } else {
            result.message = "FAILED:  Expected value " + result.expectedValue + " did not match actual value  " + result.actualValue + "\n";
        }
        return result;
    }

    public static TestResult createAssertEqualsResult(String expectedName, String expected, String actualName, String actual) {
        TestResult result = new TestResult();
        result.actualValue = actual;
        result.expectedValue = expected;
        result.expectedValueName = expectedName;
        result.actualValueName = actualName;

        result.compareValues();
        if (result.result) {
            result.message = "PASSED: Expected  " + result.expectedValueName + " value of " + result.expectedValue + " matched actual " + result.actualValueName + " value  " + result.actualValue + "\n";
        } else {
            result.message = "FAILED:  Expected  " + result.expectedValueName + " value of " + result.expectedValue + " did not match actual " + result.actualValueName + " value  " + result.actualValue + "\n";
        }
        return result;
    }

    public static class TestResult {
        public String message = "";
        public String expectedValueName = "";
        public String expectedValue = "";
        public String actualValue = "";
        public String actualValueName = "";
        public boolean result;

        //build your own
        public TestResult() {

        }

        //build a dummy result with just a message
        public TestResult(String message, boolean result) {
            this.result = result;
            this.message = message;
        }


        public void compareValues() {
            result = expectedValue.equals(actualValue);
        }
    }
}
