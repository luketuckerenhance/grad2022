package nz.co.enhance.automationFramework.HelperClasses;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.List;

public class JSONParser {

    public JsonElement rootJsonElement;

    public JSONParser(String json) {
        JsonParser parser = new JsonParser();
        rootJsonElement = parser.parse(json);
    }


    // Define the full path to the element you want the value of in the notation node.node.elementName
    // E.g. widget.window.name
    // Use for instances where you have only a single possible result
    public String findValue(String elementPath) {
        String[] nodes = elementPath.split("\\.");
        List<String> nodeList = HelperMethods.turnStringArrayIntoList(nodes);
        return getValue(rootJsonElement, nodeList);

    }

    // Define the full path to the element you want the value of in the notation node.node.elementName
    // E.g. customer.contact.phoneNumber
    // Use for instances where you may have multiple results that match the path, then pick the one you want from the
    // list.
    // You can also use it to count how many instances of a particular element are in the response.
    public List<String> findValues(String elementPath) {
        String[] nodes = elementPath.split("\\.");
        List<String> nodeList = HelperMethods.turnStringArrayIntoList(nodes);
        return getValues(rootJsonElement, nodeList);
    }

    public String getTypeOfElement(String elementPath) {
        String[] nodes = elementPath.split("\\.");
        List<String> nodeList = HelperMethods.turnStringArrayIntoList(nodes);
        return getType(rootJsonElement, nodeList);

    }

    public String toJsonString() {
        return rootJsonElement.toString();
    }

    private String getValue(Object jsonObject, List<String> nodeList) {
        if (jsonObject.getClass().toString().contains("JsonObject")) {
            String element = nodeList.get(0);
            JsonObject job = (JsonObject) jsonObject;
            if (job.keySet().contains(element)) {
                nodeList.remove(0);
                return getValue(job.get(element), nodeList);
            }
        }
        if (jsonObject.getClass().toString().contains("JsonArray")) {
            JsonArray jar = (JsonArray) jsonObject;
            for (Object obj : jar) {
                return getValue(obj, nodeList);
            }
        }
        if (jsonObject.getClass().toString().contains("JsonPrimitive")) {
            return ((JsonPrimitive) jsonObject).getAsString();
        }
        return "The value could not be found";
    }

    private List<String> getValues(Object jsonElement, List<String> nodeList) {
        List<String> values = new ArrayList<>();
        if (jsonElement.getClass().toString().contains("JsonObject")) {
            String element = nodeList.get(0);
            JsonObject job = (JsonObject) jsonElement;
            if (job.keySet().contains(element)) {
                //we don't want to modify the original nodelist as we may be checking lots of objects in an array
                List<String> newNodeList = new ArrayList<>(nodeList);
                newNodeList.remove(0);
                values.addAll(getValues(job.get(element), newNodeList));
                return values;
            }
        }
        if (jsonElement.getClass().toString().contains("JsonArray")) {
            JsonArray jar = (JsonArray) jsonElement;
            for (Object obj : jar) {
                values.addAll(getValues(obj, nodeList));
            }
            return values;
        }
        if (jsonElement.getClass().toString().contains("JsonPrimitive")) {
            values.add(((JsonPrimitive) jsonElement).getAsString());
            return values;

        }
        return values;
    }

    private String getType(Object jsonObject, List<String> nodeList) {
        if (jsonObject.getClass().toString().contains("JsonObject")) {
            String element = nodeList.get(0);
            JsonObject job = (JsonObject) jsonObject;
            if (job.keySet().contains(element)) {
                nodeList.remove(0);
                return getType(job.get(element), nodeList);
            }
        }
        if (jsonObject.getClass().toString().contains("JsonArray")) {
            JsonArray jar = (JsonArray) jsonObject;
            for (Object obj : jar) {
                return getType(obj, nodeList);
            }
        }

        //Todo refine for different types of numbers and dates
        if (jsonObject.getClass().toString().contains("JsonPrimitive")) {
            if (((JsonPrimitive) jsonObject).isString()) {
                return "string";
            } else if (((JsonPrimitive) jsonObject).isNumber()) {
                return "number";
            } else if (((JsonPrimitive) jsonObject).isBoolean()) {
                return "boolean";
            } else {
                return "The object type could not be determined";
            }
        }
        return "The object type could not be determined";
    }

    // This is used when you have an array of data objects and want to check/extract data from each one
    // rather than specifying which node by jsonpath. You need to specify the parent node.

    public List<JSONParser> getListOfObjects(String parentNode) {
        JsonArray jsonArray = (JsonArray) rootJsonElement.getAsJsonObject().get(parentNode);
        List<JSONParser> listOfObjectsInArray = new ArrayList<>();
        for (JsonElement obj: jsonArray) {
            listOfObjectsInArray.add(new JSONParser(obj.toString()));
        }
        return listOfObjectsInArray;
    }
}
