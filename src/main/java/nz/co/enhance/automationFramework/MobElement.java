package nz.co.enhance.automationFramework;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.HideKeyboardStrategy;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.Assert.assertFalse;

public class MobElement extends Element {
    public By locator = null;
    public MobileElement mobileElement = null;

    //when instantiating the element we DON'T fetch it. This is so we can set them up without them being visible yet.
    public MobElement(By locator) {
        super(locator);
        this.locator = locator;
    }

    public MobElement(MobileElement element) {
        super(element);
        mobileElement = element;
    }


    //Waits x seconds for the element to exist - note does not check visibility
    @Override
    public void waitForElementToExist(int seconds) {
        int i = 0;
        while (i < seconds) {
            if (exists()) {
                return;
            } else {
                HelperMethods.sleep((long) 50);
                i++;
            }
        }
        try {
            throw new Exception("Element does not exist after " + seconds + " seconds.");
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(false);
        }
    }

    @Override
    public MobileElement returnElementWhenExists(int seconds) {
        waitForElementToExist(seconds);
        return mobileElement;
    }

    @Override
    public MobileElement returnElementWhenDisplayed(int seconds) {
        waitForElementToBeDisplayed(seconds);
        return mobileElement;
    }

    //Waits x seconds for the element to be displayed
    @Override
    public void waitForElementToBeDisplayed(int seconds) {
        int i = 0;
        if (this.locator == null) { //if we've made this with an element then we check if it's displayed
            if (mobileElement.isDisplayed()) {
                return;
            }
        } else {
            while (i < (seconds * 5)) {
                if (isElementDisplayed()) {
                    return;
                } else {
                    HelperMethods.sleep((long) 50);
                    i++;
                }
            }
            //we only get to here if the element is not displayed
            try {
                throw new AssertionError("Element is not displayed after " + seconds + " seconds.");
            } catch (Exception e) {
                e.printStackTrace();
                assert (false);

            }
        }
    }

    public MobileDriver getDriver() {
        if (Automator.driver.getClass().toString().toLowerCase().contains("iosdriver")) {
            return (IOSDriver) Automator.driver;
        } else {
            return (AndroidDriver) Automator.driver;
        }
    }

    public String getDriverType() {
        if (Automator.driver.getClass().toString().toLowerCase().contains("iosdriver")) {
            return "ios";
        } else {
            return "android";
        }
    }


    //Checks for existence
    @Override
    public boolean exists() {
        if (this.mobileElement != null) {
            return mobileElement.isDisplayed();
        } else {
            List<WebElement> elements = Automator.driver.findElements(this.locator);
            if (elements.size() > 0) {
                mobileElement = (MobileElement) elements.get(0);
                return true;
            } else {
                return false;
            }
        }
    }

    //Checks for visibility
    public boolean isElementDisplayed() {
        List<WebElement> elements = Automator.driver.findElements(this.locator);
        if (elements.size() > 0) {
            if (elements.get(0).isDisplayed()) {
                mobileElement = (MobileElement) elements.get(0);
                return true;
            }
        }
        return false;
    }

    public IOSElement findIOSElement() {
        if (locator == null) {
            return (IOSElement) mobileElement;
        } else {
            return (IOSElement) ((IOSDriver) Automator.driver).findElement(locator);
        }
    }

    public AndroidElement findAndroidElement() {
        if (locator == null) {
            return (AndroidElement) mobileElement;
        } else {
            return (AndroidElement) ((AndroidDriver) Automator.driver).findElement(locator);
        }
    }

    //retrieves the webelement if it exists, otherwise return null
    @Override
    public MobileElement findElement() {
        if (mobileElement != null) {
            if (getDriverType().equals("ios")) {
                return findIOSElement();
            } else {
                return findAndroidElement();
            }
        } else {
            List<MobileElement> elements = findMobileElements(locator);
            if (elements.size() > 0) {
                mobileElement = elements.get(0);
                return findElement();
            }
            //if we get to here, it couldn't be found
            try {
                throw new Exception("Element does not exist.");
            } catch (Exception e) {
                System.out.println("Element existence check returned false");
            }
            return null;
        }

    }

    private List<MobileElement> findMobileElements(By locator) {
        if (getDriverType().equals("ios")) {
            return ((IOSDriver) getDriver()).findElements(locator);
        } else {
            return ((AndroidDriver) getDriver()).findElements(locator);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                          Basic commands                                                        //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public void click() {
        findElement().click();
    }

    //to help mobile testers use correct terminology
    @Override
    public void tap() { //does a click but named for mobile
        click();
    }


    //used for IOS as it pastes in the value rather than tapping each key
    public void setValue(String string) {
        if ((Automator.driver.getClass().toString().toLowerCase().contains("iosdriver"))) {
            IOSElement element = (IOSElement) findElement();
            element.setValue(string);
        } else {
            findElement().sendKeys(string);
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            Swipes/Scrolls/Touches                                                  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void dismissKeyboard() {
        if (getDriverType().equals("ios")) {
            ((IOSDriver) Automator.driver).hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "Done");
        } else {
            ((AndroidDriver) Automator.driver).pressKey(new KeyEvent(AndroidKey.BACK));
        }
    }

    //to help mobile testers use correct terminology
    public void swipeDown() {
        flickDown();
    }


}
