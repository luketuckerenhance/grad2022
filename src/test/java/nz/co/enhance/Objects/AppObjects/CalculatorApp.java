package nz.co.enhance.Objects.AppObjects;

import nz.co.enhance.automationFramework.LocatorType;
import nz.co.enhance.automationFramework.WinElement;

public class CalculatorApp {
    public WinElement plusSign = new WinElement(LocatorType.ACCESSIBILITYID, "plusButton");
    public WinElement equalsSign = new WinElement(LocatorType.ACCESSIBILITYID, "equalButton");
    public WinElement resultsField = new WinElement(LocatorType.ACCESSIBILITYID, "CalculatorResults");

    public CalculatorApp() {
        resultsField.waitForElementToBeDisplayed(5);
    }

    public void clickNumberButton(int number) {
        new WinElement(LocatorType.ACCESSIBILITYID, "num" + number + "Button").click();
    }
}
