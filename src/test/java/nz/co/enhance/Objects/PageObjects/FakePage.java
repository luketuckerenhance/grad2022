package nz.co.enhance.Objects.PageObjects;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class FakePage {

    //Elements are defined first
    public Element emailField = new Element(By.id("email"));
    public Element passwordField = new Element(By.id("password"));
    public Element loginButton = new Element(By.id("login"));

    //Constructor is directly under elements
    public FakePage() {
        emailField.waitForElementToExist(20); // Must have a fluent wait for a defining element
    }

    //All other page-specific methods go below the constructor
    public String fetchFakeUsername(String type) {
        return "Murphy";
    }

}
