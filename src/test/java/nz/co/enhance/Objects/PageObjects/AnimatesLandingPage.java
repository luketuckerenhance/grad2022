package nz.co.enhance.Objects.PageObjects;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class AnimatesLandingPage {

    public Element heroBanner = new Element(By.className("hp-hero"));
    public Element searchIcon = new Element(By.className("search-toggle"));
    public Element searchInput = new Element(By.id("search"));


    public AnimatesLandingPage(){
        heroBanner.waitForElementToExist(20);
    }

}
