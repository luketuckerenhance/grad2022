package nz.co.enhance.Objects.PageObjects;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class AnimatesSearchResultsPage {

    public Element topResultTitle = new Element(By.xpath("//span[text()='Top Result']"));

    public AnimatesSearchResultsPage() {
        topResultTitle.waitForElementToExist(10);
    }

    public Element getResultLink(String text) {
        return new Element(By.partialLinkText(text));
    }
}
