package nz.co.enhance.Objects.DataObjects;

public class TMFavouriteSearchBody {
    // When constructing an object to model an API request body make sure you use the exact names of the fields include the same case.
    // You can structure the data by using Lists or other child objects.
    // Make sure the fields are the correct type to match the expected request body.

    public String Email;
    public String SearchString;
    public String Type;
}
