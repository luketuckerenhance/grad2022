package nz.co.enhance.Objects.DataObjects;

public class ExampleEmployeeRecord {
    public String name;
    public String salary;
    public String age;

    // When constructing an object to model an API request body make sure you use the exact names of the fields.
    // You can structure the data by using Lists or other child objects.
}


