package nz.co.enhance.Objects.ProxyExampleObjects;

import nz.co.enhance.automationFramework.Element;
import nz.co.enhance.automationFramework.ElementList;
import org.openqa.selenium.By;

public class UserProfilePage {
    //profile selection
    public static Element chooseProfileGrid = new Element(By.cssSelector("div.choose-profile__grid"));
    public ElementList profileAvatars = new ElementList(By.cssSelector("li.profile-avatar"));


    public UserProfilePage() {
        chooseProfileGrid.waitForElementToExist(10);
    }


    public boolean doesProfileExist(String userProfile) {
        for (Element profile : profileAvatars.getElementList()) {
            if (profile.findChild(By.cssSelector("span.profile-avatar__title")).getText().equalsIgnoreCase(userProfile)) {
                return true;
            }
        }
        return false;
    }


}
