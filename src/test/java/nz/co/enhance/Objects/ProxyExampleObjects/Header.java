package nz.co.enhance.Objects.ProxyExampleObjects;


import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class Header {

    public static Element headerContainer = new Element(By.className("Header-container"));
    public static Element logo = new Element(By.className("SiteNav-tvnz-logo-img"));
    public static Element kidsLogo = new Element(By.className("SiteNav-tvnz-kids-logo-img"));

    // AB Test Elements: Live TV & Guide, On Demand, What To Watch, 1 News Now
    public Element liveTVLink = new Element(By.linkText("Live TV & Guide"));
    public static Element onDemandLink = new Element(By.linkText("OnDemand"));
    public Element whatToWatchLink = new Element(By.linkText("What to Watch"));
    public Element oneNewsNowLink = new Element(By.cssSelector(".SiteNav-item > a[href*=\"one-news\"]"));
    public Element mobileOneNewsNowLink = new Element(By.cssSelector(".SideMenu-primary-list a[href*=\"one-news\"]"));
    public Element closeIcon = new Element(By.className("icon-close"));

    // AB Test Elements: Home, My Favorites, Categories, A-Z, Live TV
    public Element homeLink = new Element(By.linkText("Home"));
    public Element myFavoritesLink = new Element(By.cssSelector(".SiteNav-item > a[href=\"/categories/my-favourites\"]"));
    public Element categoriesLink = onDemandLink;
    public Element atozLink = new Element(By.cssSelector("href=\"/categories/all\""));

    public Element loginLink = new Element(By.cssSelector("[data-test-common-header-login-button]"));
    public Element mobileLoginLink = new Element(By.xpath("//button[text()=\"Login\"]"));
    public Element searchIcon = new Element(By.cssSelector("span.icon.icon-search"));
    public Element searchBar = new Element(By.cssSelector("[placeholder=\" Search Shows\"]"));
    public Element currentProfileUser = new Element(By.cssSelector("div.User-current-profile-name-ellipsis"));
    public Element headerProfileButton = new Element(By.cssSelector("div[data-test-common-header-profile-button] > div.User-current-profile"));
    public Element profileUserDropdownList = new Element(By.cssSelector("div.User-dropdown.User-dropdown-profiles "));
    public Element myFavouritesDropDownMenu = new Element(By.linkText("My Favourites"));
    public Element settingsDropDownMenu = new Element(By.linkText("Settings"));
    public Element manageProfileDropDownMenu = new Element(By.linkText("Manage Profiles"));
    public Element helpDropDownMenu = new Element(By.linkText("Help"));
    public Element accountDropDownMenu = new Element(By.linkText("Account"));
    public Element logOutDropDownMenu = new Element(By.linkText("Log out"));
    public Element logOutProfileDropDownMenu = new Element(By.linkText("Logout"));
    public Element exitKidsDropDownMenu = new Element(By.linkText("EXIT KIDS"));
    public Element exitPreschoolDropDownMenu = new Element(By.linkText("EXIT PRESCHOOL"));
    public Element userNavigationList = new Element(By.cssSelector("ul.SiteNav-items"));
    public Element userDropdownList = new Element(By.cssSelector("ul.User-dropdown-list"));
    public Element kidChangePictureButton = new Element(By.className("User-dropdown-profiles-item-title-subtitle"));
    public Element kidProfileImage = new Element(By.cssSelector("div.User-current-profile > img"));

    public Element headerNavigationList = new Element(By.cssSelector("ul.SiteNav-items.SiteNav-items--left"));
        public Element categoriesUserNavSubList = new Element(By.cssSelector("div.SiteNav-sub > ul.SiteNav-sub-list.categories"));

    //Mobile Elements
    public Element siteNavHamburger = new Element(By.className("SiteNav-hamburger"));
    public Element mobileLoginButton = new Element(By.cssSelector(".SideMenu-login > button"));

    public String getTVNZLogoLink() {
        return logo.getParent().getAttribute("href");
    }


}
