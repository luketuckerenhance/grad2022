package nz.co.enhance.Objects.ProxyExampleObjects;


import nz.co.enhance.automationFramework.Automator;
import nz.co.enhance.automationFramework.Element;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;

public class EpisodePage {


    public Element video = new Element(By.className("vjs-tech"));
    public Element closeVideoButton = new Element(By.cssSelector("a.Button.Hero-close-action"));

    public Element showTitle = new Element(By.cssSelector("div.Hero-title > a"));
    public Element videoFavouriteButton = new Element(By.cssSelector("div.Hero-actions *> span.scod-FavouriteButton--small"));

    public Element unfavouriteButton = new Element(By.cssSelector("span.scod-FavouriteButton.not-favourited"));
    public Element isfavouriteButton = new Element(By.cssSelector("span.scod-FavouriteButton.is-favourited"));

    public Element adMessage = new Element(By.className("vjs-ad-message-text"));
    public Element loadingSpinner = new Element(By.className("vjs-loading-spinner"));

    public Element playerContainer = new Element(By.cssSelector("div.Player.Grid > div > div > div > div.Player-container"));
    public Element playerGuardContent = new Element(By.cssSelector("div.Player-guard.content"));

    // Player Control
    public Element videoControlsBar = new Element(By.cssSelector("div.vjs-control-bar"));

    // Play Control
    public Element videoPlayControl = new Element(By.cssSelector("button.vjs-play-control"));
    public Element bigPlayButton = new Element(By.cssSelector("button[title='Play Video']"));
    public Element smallPlayButton = new Element(By.cssSelector("button.vjs-play-control.vjs-control.vjs-button.vjs-paused"));
    public Element pauseButton = new Element(By.cssSelector("button.vjs-play-control.vjs-control.vjs-button.vjs-playing"));
    public Element replayButton = new Element(By.cssSelector("button[title='Replay']"));

    // Volume Control
    public Element videoVolumeControl = new Element(By.cssSelector("div.vjs-volume-panel"));
    public Element muteButton = new Element(By.cssSelector("div.vjs-volume-panel.vjs-control.vjs-volume-panel-horizontal > button"));
    public Element volumeBarSlider = new Element(By.cssSelector("div.vjs-volume-bar.vjs-slider-bar"));

    public String getCurrentVolume() {
        return volumeBarSlider.getAttribute("aria-valuenow");
    }

    // Progress Control
    public Element videoProgressControl = new Element(By.cssSelector("div.vjs-progress-control"));
    public Element videoPlayProgressSlider = new Element(By.cssSelector("div.vjs-progress-holder.vjs-slider"));

    // Time Control
    public Element videoCurrentTime = new Element(By.cssSelector("span.vjs-current-time-display"));
    public Element videoCurrentTimeDev = new Element(By.cssSelector("div.vjs-current-time-display"));
    public Element videoDurationTime = new Element(By.cssSelector("span.vjs-duration-display"));

    public Element videoShareButton = new Element(By.cssSelector("button.vjs-icon-share"));
    public Element videoFullscreenButton = new Element(By.cssSelector("button[title='Toggle Fullscreen']"));

    // Caption Control
    public Element subtitleCaption = new Element(By.cssSelector("div.vjs-subs-caps-button"));
    public Element subCapsButton = new Element(By.cssSelector("button.vjs-subs-caps-button"));

    // Sponsored Advertisement
    public Element advertisementOnPause = new Element(By.cssSelector("div.AdOnPause"));
    public Element adOnPauseCloseButton = new Element(By.cssSelector("button.AdOnPause-close"));

    // Collapsed Player
    public Element collapsedPlayerContainer = new Element(By.cssSelector("div.Grid.Player--collapsed"));
    public Element dynamicPlayerVideo = new Element(By.cssSelector("div.Player-video-clickInterceptor"));
    public Element dynamicPlayerEpisodeTitle = new Element(By.cssSelector("div.Player-container *> div.Player-next-title"));
    public Element sidebarPlayerEpisodeTitle = new Element(By.cssSelector("div.Player-sidebar *> div.Player-next-title"));

    // Player sidebar
    public Element playerSidebarContainer = new Element(By.cssSelector("div.Player-sidebar"));
    public Element psTitle = new Element(By.cssSelector("h1.Player-title"));
    public Element psRating = new Element(By.cssSelector("span.Player-rating"));
    public Element psSynopsis = new Element(By.cssSelector("span.Player-synopsis"));
    public Element psNextTitle = new Element(By.cssSelector("div.Player-next-title"));
    public Element psNextMeta = new Element(By.cssSelector("div.Player-next-meta"));

    // Player Overlay
    public Element playerOverlayContainer = new Element(By.cssSelector("div.PlayerOverlay"));
    public Element playerOverlayCloseButton = new Element(By.cssSelector("div.PlayerOverlay-close"));
    public Element playerOverlayDirectLinkField = new Element(By.cssSelector("input.PlayerOverlay-link"));
    public Element playerOverlayWatchCreditsButton = new Element(By.xpath("//span[contains(text(),'Watch Credits')]"));
    public Element playerOverlayNextEpisodePlayButton = new Element(By.cssSelector("div.PlayerOverlay *> span.Tile-playButton"));

    public Element playerOverlayItemsParent = new Element(By.cssSelector("ul.PlayerOverlay-items"));
    public By byPlayerOverlayChild = By.cssSelector("li.PlayerOverlay-item");

    // Social Media
    public Element facebookIcon = new Element(By.cssSelector("i.icon-facebook"));
    public Element messengerIcon = new Element(By.cssSelector("i.icon-messenger"));
    public Element twitterIcon = new Element(By.cssSelector("i.icon-twitter"));
    public Element mailIcon = new Element(By.cssSelector("i.icon-mail"));

    // Related Contents
    public Element episodeRowParent = new Element(By.cssSelector("div[data-module-name=\"episode-row\"]"));
    public By byRelatedContentBeltTitle = By.cssSelector("div.Belt-title");
    public By byNextEpisodeMeta = By.cssSelector("div.Episode-title");

    public Element featuredContentParent = new Element(By.cssSelector("div[data-module-name=\"featuredContent\"]"));
    public By byFeaturedContentShows = By.cssSelector("li.Belt-item");

    // Live Episode
    public Element playerGuardButtonContainer = new Element(By.cssSelector("div.Player-guard-buttons"));
    public Element watchFromStartButton = new Element(By.xpath("//a[contains(text(),'Watch from the start')]"));
    public Element watchLiveButton = new Element(By.xpath("//a[contains(text(),'Watch Live')]"));
    public Element episodeVideoLiveButton = new Element(By.cssSelector("button.vjs-live-button"));

    public List<Element> getMidRollMarkers() {
        bringUpActionBar();
        HelperMethods.sleep(2);
        return videoProgressControl.findChildElements(By.cssSelector("div.vjs-marker"));
    }

    public List<Element> getPlayerOverlayItems() {
        if (playerOverlayItemsParent.exists(10)) {
            return playerOverlayItemsParent.findChildElements(byPlayerOverlayChild);
        } else {
            return new ArrayList<>();
        }
    }

    public void bringUpActionBar() {
        Actions actions = new Actions(Automator.driver);
        Actions moveto = actions.moveToElement(video.findElement()).moveByOffset(50, 50).moveToElement(video.findElement());
        moveto.perform();
    }

    public Element getSubtitleMenuElement(String subtitleMenu) {
        if (subtitleCaption.exists()) {
            for (Element menuItem : subtitleCaption.findChildElements(By.cssSelector("*> li.vjs-menu-item"))) {
                if (menuItem.getText().equalsIgnoreCase(subtitleMenu)) {
                    return menuItem;
                }
            }
        }
        return null;
    }

    public List<Element> getRelatedContentShows() {
        return featuredContentParent.findChildElements(byFeaturedContentShows);
    }

    public String getNextEpisodeBeltTitle() {
        return episodeRowParent.findChild(byRelatedContentBeltTitle).getText();
    }

    public String getRelatedContentTitle() {
        return featuredContentParent.findChild(byRelatedContentBeltTitle).getText();
    }

    public String getNextEpisodeMeta() {
        return episodeRowParent.findChild(byNextEpisodeMeta).getText();
    }

    public String getVideoPlayProgress() {
        return videoPlayProgressSlider.getAttribute("aria-valuenow");
    }

    public EpisodePage() {
        // check for individual elements also as the video playback could be in several states
        playerContainer.waitForElementToBeDisplayed(60);
    }
}
