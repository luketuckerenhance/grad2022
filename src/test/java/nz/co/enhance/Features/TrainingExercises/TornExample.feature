@api
Feature: Uses the Torn City API to run status checks

  Scenario: I check my personal stats on Torn
    When I sent a GET request to the Torn API with the tag personalstats
    Then I see the response code from Torn is 200
    And my stats contain the following:
      | fieldName       | value |
      | bazaarcustomers | 235   |
      | meritsbought    | 10    |
      | defendswon      | 209   |

    #TODO: make "my stats contain..." word for any field (personalstats, properties, money, log)