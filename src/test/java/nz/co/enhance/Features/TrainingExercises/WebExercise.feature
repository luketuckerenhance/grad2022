#  Web Exercise
#
# Create a set of several scenarios to test the website: https://www.tmsandbox.co.nz/
# You need to cover the following:
# - Create a happy path flow for a basic functionality of your choice (e.g. actions such as logging in, searching for a product, browsing a category, creating a new user etc)
# - Create a scenario outline for several tests that do the same action but with different inputs and results
# - Create a negative test
# - Test your Scenarios across at least 2 browsers
# - Tag your Scenarios appropriately
#
#
#
# In order to create good tests make sure you do the following:
# - Generate reusable, concise step defs (see the guide to writing good cucumber)
# - Create Page Objects that are suitably defined for the pages being tested
# - Ensure you use Junit assertions correctly and appropriately (all tests must have at least one assertion!)
# - Ensure your tests are specific to what is actually being tested, and do not try to test multiple different outcomes
#   - e.g. a login test may perform a successful login and assert that you can see a welcome message, your user name and the
#    time of your login, however other tests that perform logins as part of their steps should not assert for these things as
#    they are only relevant to a login test. If you assert them in every test then you might make every test in the suite fail
#    if for instance the user name is not displayed upon a successful login.
# - Keep all locators in Page Objects not step defs.
# - Try to use these locator types in order of preference: id, name, linkText, className, cssSelector, xpath. Xpath is always the last resort.
# - Only reference a locator ONCE, not in multiple page objects. If there is something common to multiple pages (eg a nav
#   bar or menu) then create an object for that and either reference it directly in the step def or include it as one of the objects within a Page Object.
# - Put incorrect data through your tests to be sure your assertions are working correctly.
#
# Helpful links:
#
# Guide to writing good Cucumber: https://saucelabs.com/blog/write-great-cucumber-tests

