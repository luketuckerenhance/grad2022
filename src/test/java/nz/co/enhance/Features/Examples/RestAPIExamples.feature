@api
Feature: Examples of different API tests using the TradeMe sandbox APIs

  This uses the Enhance library to set up request objects and send and receive data. RestAssured is a similar library
  that is often used by devs so there are examples using this in another feature file, but this is the preferred method
  for API testing in Enhance projects.

  All examples are using Rest APIs because SOAP APIs (using XML) are relatively rare nowadays, however the basic actions would
  remain the same using SOAP except for sending and parsing XML instead of JSON.


  # A get scenario is quite simple to test.
  # Get requests only require an endpoint and any headers that are applicable (sometimes there aren't any required headers if the API is public)
  # The endpoint may contain a query parameter or parameters, which you would send through from the step defs
  # The response is parsed to check the testing conditions are fulfilled, which would include such checks as:
  #   - the response matches the schema (fields, structure, data types)
  #   - the data returned matches our expected data for the request sent, e.g. the correct user details were returned, or the correct
  #     number of records

  Scenario: Send a simple GET request and validate the response
    When I send a GET request to the TradeMe Sandbox Retrieve Jobs Categories API
    Then I see the response code is 200
    And I see the response contains the following data:
      | fieldName          | expectedResult |
      | Name               | Accounting     |
      | Code               | 5001           |
      | SubCategories.Name | Accountants    |
      | SubCategories.Code | 5002           |


  #Often we send parameters with our GET requests to specify what data we are looking for.
  Scenario: Send a GET request with parameters and validate the response
    When I send a GET request to the TradeMe Sandbox Categories API with the following parameters:
      | mcat_path | pets-animals |
    Then I see the response code is 200
    And I see the response contains the following data:
      | fieldName          | expectedResult |
      | CategoryId         | 9425           |
      | CanListAuctions    | false          |
      | CanListClassifieds | false          |
      | Path               | /Pets-animals  |


  # Many APIs need to be authenticated to be accepted.
  # Often this is done by a user logging in and a token being issued for that session.
  # Sometimes we have a permanent token that can be reused, as in this example.
  # All auth is done in headers.
  # Keep in mind that static tokens should be kept in properties files NOT in scenarios - this is just to show how they work.
  # The POST request below this one shows the correct way to handle auth tokens.
  Scenario: A GET request with Oauth authorisation
    When I send a GET request to the TradeMe Sandbox MyTradeMe Summary API with the following authentication details:
      | oauth_consumer_key | 6AAC5D1C897A0BFC017EC81892B723C4                                  |
      | oauth_token        | 22C15BA7ECA76D8C25962D8A07948248                                  |
      | oauth_signature    | 82B3B580410B571A1009413595F08DBF&C162839FE79094F5EC590E80BC49D220 |
    Then I see the response code is 200
    And I see the response contains the following data:
      | fieldName              | expectedResult |
      | Nickname               | bronwynenhance |
      | MemberId               | 4007184        |
      | MembershipAddress.Name | Bronwyn Dent   |


  # A POST request is slightly more work to set up.
  # POSTs will require an endpoint (usually they don't have parameters), headers and a body (always JSON for REST).
  #
  # We send the body as a string and we always use objects to model the data.
  # The below example shows how to model the request with an object then convert the object to JSON to form the body of
  # the POST.
  #
  # Because you are sending data you usually have to be authenticated so this example uses authentication and it is
  # managed using properties files, as all authentication should be!

  Scenario: Create a POST request using objects to model the request body
    When I send a POST request for user exampleTMUser to the TradeMe Sandbox Favourite Search API with the following details:
      | fieldName    | value    |
      | Email        | 7        |
      | SearchString | Vallhund |
      | Type         | 1        |
    Then I see the response code is 200
    And I see the response contains the following data:
      | fieldName     | expectedResult                            |
      | Response      | You are already subscribed to this search |
      | Saved         | true                                      |
      | FavouriteType | 4                                         |

