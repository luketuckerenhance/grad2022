Feature: Tests to buy treats for dogs from Animates

  Scenario: Search for a dog treat with the text hooves
    Given I navigate to the Animates website
    When I search for an Animates product with the text hooves
    Then I see a top result with the title Butchers Superior Cuts Beef Hooves Dog Treat 5 Pack

  Scenario: Search for a dog treat with the text cat
    Given I navigate to the Animates website
    When I search for an Animates product with the text cat
    Then I see a top result with the title Go Cat Tail Colourful Feather Wand Cat Toy 36inch