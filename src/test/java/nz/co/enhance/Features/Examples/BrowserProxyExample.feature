@browserproxy
Feature: POC for Analytics testing using Segment

  This test shows how we can check the traffic going in and out of a browser to ensure that particular calls, e.g.
  analytics api calls, are correctly being triggered from the front end. It requires using the chromeProxy runTarget
  otherwise the proxy will not start up. It does not currently work for other browsers.

  You would only use this type of test to specifically check that browser actions trigger the right calls. You would not
  use this to test API functionality as it is easier and faster to test that directly against API endpoints.

  You can write out the HAR log if you wish to, for debugging purposes, but this is commented out in this example so
  that repeated running does not create extra files.

  To run this test your runTarget in global.properties must be set to chromeProxy

  Scenario: Sniffing Analytics calls from TVNZ
    Given I log in to OnDemand
    And I navigate to a VOD episode
    When I click the play button on the VOD episode video
    Then I see the correct analytics call for a VOD play event
    #And I write out the HAR log
