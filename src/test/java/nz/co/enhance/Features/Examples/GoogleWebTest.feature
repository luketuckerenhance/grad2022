@web
Feature: Simple web test examples using Google

  # The most basic of scenarios. The highlighted words are variables and can be replaced with any text you would like to
  # test.
  @search
  Scenario: Perform a google search for a specified term
    Given I navigate to the Google URL
    When I search for the term Panda
    Then I see there is a search result with the text Giant panda


  # This demonstrates a simple Scenario Outline. You would use this when you had to run the same set of actions repeatedly
  # with different inputs and results. A common example of this is testing login combinations.
  @search2
  Scenario Outline: Perform a google search for multiple terms using Scenario Outline
    Given I navigate to the Google URL
    When I search for the term <searchTerm>
    Then I see there is a search result with the text <expectedResult>
    Examples:
      | searchTerm | expectedResult                         |
      | Panda      | Giant panda                            |
      | origami    | Easy Origami Instructions and Diagrams |


  # This is a very simple single column data table so one Step Def can be given a list of items to check rather than repeat
  # the step def over and over. The scenario only runs once but checks all things in the list.

  @search3
  Scenario: Perform a google search and check the results for multiple words with a simple data table
    Given I navigate to the Google URL
    When I search for the term Corgis
    Then I see the search results contain the following words:
      | corgi       |
      | dog         |
      | Pembroke    |
      | Welsh       |
      | intelligent |


  # This is a more complicated data table with multiple rows and columns. When using this type of data table it's a
  # good idea to make the top row labels to indicate what that column contains. Can be used for checking more complicated
  # outcomes esp. things like API results when you want to check fields and values as a pair rather than just search
  # the entire response for a word.

  @search4
  Scenario: Perform a google search and check multiple results for multiple attributes with a complex data table
    Given I navigate to the Google URL
    When I search for the term Corgis
    Then I see the following search results contain the terms:
      | resultTitle                                | resultContainsTerm |
      | Pembroke Welsh Corgi Dog Breed Information | herd               |
      | Pembroke Welsh Corgi - Wikipedia           | Pembrokeshire      |
