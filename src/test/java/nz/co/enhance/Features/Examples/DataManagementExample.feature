@datamanagement
Feature: Good strategies for managing data variables

  Tests use data that can change between environments or database refreshes, and hard coding this into a test
  is not a good idea. It is better to centralise the data in a way that can be easily updated. We can do this in lots of
  ways however here are two examples.

  - Data stored in Json files and converted into objects
  This is best when you have multiples of the same type of data (e.g. users, products, ) especially if that data
  has multiple fields/attributes that could be modeled as an object

  - Properties files
  Normally used for endpoints, flags, URLs or individual tokens/credentials. One-to-one variable and value.

  Both of the above can be utilised in multiple environments by naming the files with the env name and then checking which
  one to load at runtime, e.g. your env=dev and your TestDriver will read the users-dev.json and endpoints-dev.properties files.

  Have a look at the code of the below tests and see /src/main/resources/userData  and /src/main/resources/tvnzData-live.properties.

  This test does both of the above examples as it gets the user from the json file and the OnDemand URL from the properties file.

  Also, this test stores the user data from the first step in the ExecutionData class so it can be passed between
  steps and accessed in the third step to assert the correct profile is shown for the specified user.


  Scenario: Use a User object to structure our TVNZ user
    When I log in to OnDemand as user defaultUser
    Then I see I am logged in to OnDemand
    And I see the correct user profile is displayed
