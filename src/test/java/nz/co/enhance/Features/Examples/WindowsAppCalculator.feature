@winapp
Feature: Test functions of the calculator app

  Scenario: Addition
    Given the calculator app has been launched
    When I add 1 and 2
    Then I see the result is 3