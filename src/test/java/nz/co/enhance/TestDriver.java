package nz.co.enhance;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.proxy.CaptureType;
import nz.co.enhance.StepDefs.Hooks;
import nz.co.enhance.automationFramework.Automator;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;
import nz.co.enhance.automationFramework.HelperClasses.PropertiesHandler;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.Inet4Address;
import java.util.Properties;

public class TestDriver {


    // Standard setup for most test projects
    public static Automator automator = new Automator();
    public String runTarget;
    public String env;
    public static ExecutionData executionData;

    // Properties you may need to read -specific to the test project you are doing, the below are from the example scenarios
    public static Properties properties;
    public static Properties tvnzProperties;
    public static Properties googleProperties;
    public static Properties nzheraldProperties;

    // Extra optional stuff - remove if your project does not need these

    // Specific to the Browser Proxy tests only
    public static BrowserMobProxy proxy;


    public TestDriver() {
        setup();
    }

    public void setup() {
        executionData = new ExecutionData();
        //properties read
        //get the runtime properties - defines which target you will run your tests against.
        properties = new PropertiesHandler().loadProperties("src/main/resources/global.properties");

        //Set the global property of runTarget, i.e. which browser, mobile device or app type we are testing.
        runTarget = setEnvironmentProperty("runTarget");

        //sets the environment we are running in - currently only used for the TVNZ-based example tests.
        env = setEnvironmentProperty("env");

        //used for the TVNZ tests
        tvnzProperties = new PropertiesHandler().loadProperties("src/main/resources/tvnzData-" + env + ".properties");

        //used for the Google tests
        googleProperties = new PropertiesHandler().loadProperties("src/main/resources/google.properties");

        //used for the NZHerald tests
        nzheraldProperties = new PropertiesHandler().loadProperties("src/main/resources/nzherald.properties");

        // This is not mandatory but useful.
        // Any tests tagged  @api will ignore the runTarget, so no more accidentally launching a browser when you just
        // want to run an API test.
        if (Hooks.scenario.getSourceTagNames().contains("@api")) {
            runTarget = "api";
        }


        // Start up the required driver for your testing as indicated in the runTarget
        if (runTarget.equalsIgnoreCase("chrome")) {
            automator.startChrome();
        }

        if (runTarget.equalsIgnoreCase("chromedocker")) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            automator.startChrome(options);
        }
        if (runTarget.equalsIgnoreCase("firefox")) {
            automator.startFirefox();
        }
        if (runTarget.equalsIgnoreCase("safari")) {
            automator.startSafari();
        }
        if (runTarget.equalsIgnoreCase("edge")) {
            automator.startEdge();
        }
        if (runTarget.equalsIgnoreCase("ie")) {
            //You must follow these steps before IE will work:
            // https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver#required-configuration

            // The automator class sets some of the required options but you can override if you wish by setting them
            // here and passing them through as InternetExplorerOptions
            automator.startIE();
        }

        //WindowsDriver
        // You need to install WindowsDriver on the execution machine
        if (runTarget.equalsIgnoreCase("winapp")) {

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("platformName", "Windows");
            caps.setCapability("deviceName", "WindowsPC");
            caps.setCapability("app", setEnvironmentProperty("winAppPath"));
            automator.startWinAppDriver(caps);
        }

        if (runTarget.equalsIgnoreCase("ios")) { //change or parameterise these to suit your requirements
            // This set of capabilities is designed for an emulator. Real device is different.
            // Note that if you add a UDID for an emulator it will prevent restarts between scenarios which is desirable
            // for speed reasons if you are executing multiple tests.
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("platformName", "iOS");
            caps.setCapability("deviceName", "iPhone 11");
            caps.setCapability("platformVersion", "13.3");
            caps.setCapability("automationName", "XCUITest");
            caps.setCapability("browserName", "Safari");
            caps.setCapability("noReset", true);
            caps.setCapability("fullReset", false);
            caps.setCapability("newCommandTimeout", 999);
            automator.startRemoteWebDriver(caps);
        }

        if (runTarget.equalsIgnoreCase("android")) { //change or parameterise these to suit your requirements
            // This set of capabilities is designed for an emulator. Real device is slightly different.

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("platformName", "android");
            caps.setCapability("deviceName", "Nexus 6 API 28");
            caps.setCapability("browserName", "Chrome");

            //We use different versions of Chromedriver for mobile
            caps.setCapability("chromedriverUseSystemExecutable", "false");
            caps.setCapability("chromedriverExecutableDir", HelperMethods.getLocalPath() + "/src/main/resources/MobileChromedriver/CD2.40");

            caps.setCapability("newCommandTimeout", 999);
            automator.startRemoteWebDriver(caps);
        }

        if (runTarget.equalsIgnoreCase("chromeProxy")) {
            proxy = new BrowserMobProxyServer();
            proxy.setTrustAllServers(true);
            proxy.start(0);
            Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
            try {
                String hostIp = Inet4Address.getLocalHost().getHostAddress();
                seleniumProxy.setHttpProxy(hostIp + ":" + proxy.getPort());
                seleniumProxy.setSslProxy(hostIp + ":" + proxy.getPort());
            } catch (Exception e) {

            }


            //add new chromeOptions as "--ignore-certificate-errors"
            ChromeOptions opts = new ChromeOptions();
            opts.setProxy(seleniumProxy);
            opts.addArguments("--ignore-certificate-errors");

            automator.startChrome(opts);
            automator.deleteAllCookies();

            proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
            proxy.newHar("SeleniumCapture");
        }

        // Once all the browsers have been set up and you have a running automator then check the flags
        if (automator.driver != null) {

            //sets if you want web screenshots to be full page or just what is currently displayed. Default is false.
            automator.takeFullPageScreenshots = setBooleanEnvironmentProperty("takeFullPageScreenshots");

        }
    }


    public String setEnvironmentProperty(String propertyName) {
        if (System.getenv().containsKey(propertyName)) {
            return System.getenv(propertyName);
        } else if (System.getenv().containsKey(propertyName.toUpperCase())) {   //some instances of windows are uppercasing runtarget
            return System.getenv(propertyName.toUpperCase());
        } else {
            return properties.getProperty(propertyName);
        }
    }

    public Boolean setBooleanEnvironmentProperty(String propertyName) {
        if (System.getenv().containsKey(propertyName)) {
            return Boolean.valueOf(System.getenv(propertyName));
        } else {
            return Boolean.valueOf(properties.getProperty(propertyName));
        }
    }

    public void cleanup() {


        //if we need any logging or extra reporting we can pop it here.
        if (automator != null) {
            automator.quit();
        }
    }
}
