package nz.co.enhance.StepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.PageObjects.AnimatesLandingPage;
import nz.co.enhance.Objects.PageObjects.AnimatesSearchResultsPage;
import nz.co.enhance.TestDriver;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertTrue;

public class AnimatesStepDefs {

    @Given("^I navigate to the Animates website$")
    public void i_navigate_to_the_Animates_website()  {
        TestDriver.automator.navigateTo("https://www.animates.co.nz");
    }

    @When("^I search for an Animates product with the text (.*)$")
    public void i_search_for_an_Animates_product_with_the_text(String searchText) {
        AnimatesLandingPage animatesLandingPage = new AnimatesLandingPage();
        animatesLandingPage.searchIcon.click();
        animatesLandingPage.searchInput.waitForElementToBeDisplayed(5);
        animatesLandingPage.searchInput.sendKeys(searchText + Keys.ENTER);
    }


    @Then("^I see a top result with the title (.*)$")
    public void iSeeATopResultWithTheTitle(String titleText) {
        AnimatesSearchResultsPage animatesSearchResultsPage = new AnimatesSearchResultsPage();
        assertTrue("There should be a search result with the link: "+ titleText, animatesSearchResultsPage.getResultLink(titleText).exists(1));
    }
}
