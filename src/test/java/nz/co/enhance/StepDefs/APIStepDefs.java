package nz.co.enhance.StepDefs;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.DataObjects.ExampleEmployeeRecord;
import nz.co.enhance.Objects.DataObjects.TMFavouriteSearchBody;
import nz.co.enhance.Objects.DataObjects.TMUser;
import nz.co.enhance.Objects.DataObjects.User;
import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;
import nz.co.enhance.automationFramework.HelperClasses.JSONParser;
import nz.co.enhance.automationFramework.ServiceClasses.GETRequest;
import nz.co.enhance.automationFramework.ServiceClasses.HTTPRequest;
import nz.co.enhance.automationFramework.ServiceClasses.POSTRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static nz.co.enhance.automationFramework.HelperClasses.FileHandler.readFileAsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class APIStepDefs {

    HTTPRequest request;

    @When("^I send a GET request to the TradeMe Sandbox Retrieve Jobs Categories API$")
    public void iSendAGETToTheRetrieveJobsTestApi() {
        String endpoint = TestDriver.properties.getProperty("retrieveJobsCategories");
        sendGETRequest(endpoint, null);
    }

    @When("^I send a GET request to the TradeMe Sandbox Categories API with the following parameters:$")
    public void iSendAGETToTheCatrgoriesTestApi(DataTable dataTable) {
        List<List<String>> data = dataTable.cells(0);

        String query = "?";
        for (List<String> queryPair : data) {
            query = query + queryPair.get(0) + "=" + queryPair.get(1) + "&";
        }
        String endpoint = TestDriver.properties.getProperty("mcatCategories") + query;
        sendGETRequest(endpoint, null);
    }

    @When("^I send a GET request to the TradeMe Sandbox MyTradeMe Summary API with the following authentication details:$")
    public void iSendAGETToTheSummaryAPI(DataTable dataTable) {
        Map<String, String> data = dataTable.asMap(String.class, String.class);

        //formulate correct authentication string for Trademe sandbox
        String authString = "OAuth oauth_consumer_key=\"" + data.get("oauth_consumer_key") + "\", oauth_token=\"" + data.get("oauth_token") + "\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"" + data.get("oauth_signature") + "\"";

        //set headers for authorisation
        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("Authorization", authString));

        String endpoint = TestDriver.properties.getProperty("myTradeMeSummary");
        sendGETRequest(endpoint, headers);
    }


    @When("^I send a POST request for user (.*) to the TradeMe Sandbox Favourite Search API with the following details:$")
    public void iSendAPOSTRequestForUserToTheTradeMeSandboxFavouriteSearchAPIWithTheFollowingDetails(String user, DataTable dataTable) {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        TMUser tmUser = getTMUser(user);

        //formulate correct authentication string for Trademe sandbox
        String authString = "OAuth oauth_consumer_key=\"" + tmUser.oauth_consumer_key + "\", oauth_token=\"" + tmUser.oauth_token + "\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"" + tmUser.oauth_signature + "\"";

        //set headers - POSTs need a type to be set as well as the authorisation
        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("Authorization", authString));
        headers.add(Arrays.asList("Content-Type", "application/json"));

        String endpoint = TestDriver.properties.getProperty("favouritesSearch");

        //create the body
        TMFavouriteSearchBody body = new TMFavouriteSearchBody();
        body.Email = data.get("Email");
        body.SearchString = data.get("SearchString");
        body.Type = data.get("Type");

        sendPOSTRequest(endpoint, HelperMethods.toJSON(body), headers);
    }

    public void sendGETRequest(String endpoint, List<List<String>> headers) {
        request = new GETRequest(endpoint, headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    public void sendPOSTRequest(String endpoint, String body, List<List<String>> headers) {
        request = new POSTRequest(endpoint, body, headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @Then("^I see the response code is (\\d+)$")
    public void iSeeTheResponseCodeIs(int code) {
        assertEquals(code, request.responseCode);
    }

    @And("^I see the response contains the text (.*)$")
    public void iSeeTheResponseContainsTheText(String text) {
        assertTrue(request.response.contains(text));
    }


    @And("^I see the response contains the following data:$")
    public void iSeeTheResponseContainsTheFollowingData(DataTable dataTable) {
        String results = "";
        boolean result = true;

        List<List<String>> data = dataTable.cells(1); //turn the table into a list, ignoring the first row

        JSONParser j = new JSONParser(request.response);
        for (List<String> pairToCheck : data) {
            String field = pairToCheck.get(0);
            String value = pairToCheck.get(1);
            String actualValue = j.findValue(field);
            if (actualValue.equalsIgnoreCase(value)) {
                results += "PASSED: Field: " + field + " had the value " + value + " \n";
            } else {
                results += "FAILED: Field: " + field + " expected the value " + value + " but actually had: " + actualValue + "\n";
                result = false;
            }
        }

        Hooks.scenario.write(results);
        assertTrue(result);
    }



    @And("^I see the response contained (\\d+) instances of field (.*)$")
    public void iSeeTheResponseContainedInstancesOfFieldId(int expectedCount, String fieldPath) {
        JSONParser j = new JSONParser(request.response);
        int actualCount = j.findValues(fieldPath).size();
        assertEquals("The field " + fieldPath + " should have a count of " + expectedCount, expectedCount, actualCount);
    }

    @And("^I see the (.*) object has the following fields:$")
    public void iSeeTheDataObjectHadTheFollowingFields(String objectName, DataTable dataTable) {
        List<List<String>> data = dataTable.cells(1);

        String results = "";
        boolean result = true;

        JSONParser j = new JSONParser(request.response);

        for (List<String> row : data) {
            String fieldName = row.get(0);
            String fieldType = row.get(1);

            String actualType = j.getTypeOfElement(objectName + "." + fieldName);

            if (actualType.equalsIgnoreCase(fieldType)) {
                results += "PASSED: Field: " + fieldName + " had the type " + fieldType + " \n";
            } else {
                results += "FAILED: Field: " + fieldName + " expected the type " + fieldType + " but actually had: " + actualType + "\n";
                result = false;
            }
        }
        Hooks.scenario.write(results);
        assertTrue(result);
    }

    //this will map the user from the JSON file into the TMUser object.
    public TMUser getTMUser(String userName) {
        String environment = TestDriver.properties.getProperty("env");

        String file = readFileAsString("src/main/resources/userData/TMusers-" + environment + ".json");
        GsonBuilder gsonBuilder = new GsonBuilder();
        Map<String, TMUser> getUser = gsonBuilder.create().fromJson(file, new TypeToken<Map<String, TMUser>>() {
        }.getType());
        return getUser.get(userName);

    }
}
