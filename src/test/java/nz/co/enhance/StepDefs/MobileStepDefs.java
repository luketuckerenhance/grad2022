package nz.co.enhance.StepDefs;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.PageObjects.NZHeraldPage;
import nz.co.enhance.TestDriver;

import static org.junit.Assert.assertTrue;

public class MobileStepDefs {

    @Then("^I see the NZ Herald home icon is visible$")
    public void iSeeTheNZHeraldHomeIconIsVisible() {
        NZHeraldPage nzh = new NZHeraldPage();
        assertTrue("The home icon should be displayed", nzh.homeButton.isDisplayed());
    }

    @When("^I navigate to the NZ Herald website$")
    public void iNavigateToTheNZHeraldWebsite() {
        TestDriver.automator.navigateTo(TestDriver.nzheraldProperties.getProperty("nzheraldURL"));
    }
}
