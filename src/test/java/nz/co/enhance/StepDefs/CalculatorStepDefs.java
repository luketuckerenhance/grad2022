package nz.co.enhance.StepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.AppObjects.CalculatorApp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class CalculatorStepDefs {
    @Given("^the calculator app has been launched$")
    public void theCalculatorAppHasBeenLaunched() {
        CalculatorApp calc = new CalculatorApp();
        assertTrue(calc.resultsField.isDisplayed());
    }

    @When("^I add (\\d+) and (\\d+)$")
    public void iAddAnd(int firstNumber, int secondNumber) {
        CalculatorApp calc = new CalculatorApp();
        calc.clickNumberButton(firstNumber);
        calc.plusSign.click();
        calc.clickNumberButton(secondNumber);
        calc.equalsSign.click();

    }

    @Then("^I see the result is (\\d+)$")
    public void iSeeTheResultIs(String result) {
        CalculatorApp calc = new CalculatorApp();
        assertEquals(result, calc.resultsField.getText().replace("Display is ", ""));
    }
}
