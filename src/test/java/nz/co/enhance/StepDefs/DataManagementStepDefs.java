package nz.co.enhance.StepDefs;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import nz.co.enhance.Objects.DataObjects.User;
import nz.co.enhance.Objects.ProxyExampleObjects.LandingPage;
import nz.co.enhance.Objects.ProxyExampleObjects.LoginAndAuthenticationPage;
import nz.co.enhance.Objects.ProxyExampleObjects.UserProfilePage;
import nz.co.enhance.TestDriver;

import java.util.Map;

import static nz.co.enhance.automationFramework.HelperClasses.FileHandler.readFileAsString;
import static org.junit.Assert.assertTrue;

public class DataManagementStepDefs {

    @And("^I log in to OnDemand as user (.*)$")
    public void iLogInToOnDemandAsUser(String userName) {
        //navigate to login
        TestDriver.automator.navigateTo(TestDriver.tvnzProperties.getProperty("onDemandURL"));

        //gets the user based on the name passed and also the env global property (e.g. users-live, users-dev etc)
        User user = getUser(userName);

        // We want to keep track of this user throughout the test in case we need the data again in other steps
        TestDriver.executionData.user = user;

        //login as the user
        LoginAndAuthenticationPage loginAndAuthenticationPage = new LoginAndAuthenticationPage();
        loginAndAuthenticationPage.emailField.sendKeys(user.userName);
        loginAndAuthenticationPage.passwordField.sendKeys(user.password);
        loginAndAuthenticationPage.loginButton.click();
    }


    //this will map the user from the JSON file into the User object.
    public User getUser(String userName) {
        String environment = TestDriver.properties.getProperty("env");

        String file = readFileAsString("src/main/resources/userData/users-" + environment + ".json");
        GsonBuilder gsonBuilder = new GsonBuilder();
        Map<String, User> getUser = gsonBuilder.create().fromJson(file, new TypeToken<Map<String, User>>() {
        }.getType());
        return getUser.get(userName);

    }

    public String getURL() {
        String environment = TestDriver.properties.getProperty("environment");
        return TestDriver.properties.getProperty("url-" + environment.toLowerCase());
    }

    @Then("^I see I am logged in to OnDemand$")
    public void iSeeIAmLoggedInToOnDemand() {
        LandingPage landingPage = new LandingPage();
        assertTrue("I should see the TVNZ logo", landingPage.logo.isDisplayed());
    }

    @And("^I see the correct user profile is displayed$")
    public void iSeeTheCorrectUserProfileIsDisplayed() {
        UserProfilePage userProfilePage = new UserProfilePage();
        String profileName = TestDriver.executionData.user.profileName; //read the user object that was selected in the first step
        assertTrue("The profile options I am offered should include one for " + profileName, userProfilePage.doesProfileExist(profileName));
    }
}
