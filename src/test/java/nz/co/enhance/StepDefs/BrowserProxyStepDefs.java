package nz.co.enhance.StepDefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import nz.co.enhance.Objects.ProxyExampleObjects.EpisodePage;
import nz.co.enhance.Objects.ProxyExampleObjects.LandingPage;
import nz.co.enhance.Objects.ProxyExampleObjects.LoginAndAuthenticationPage;
import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.HelperClasses.DateHelper;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;
import nz.co.enhance.automationFramework.HelperClasses.JSONParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BrowserProxyStepDefs {

    @And("^I log in to OnDemand$")
    public static void iLogInToOnDemandAsUser() {
        //static data only for this example, always store your data properly!
        //navigate to login
        TestDriver.automator.navigateTo(TestDriver.tvnzProperties.getProperty("onDemandURL"));


        //login as the user
        LoginAndAuthenticationPage loginAndAuthenticationPage = new LoginAndAuthenticationPage();
        loginAndAuthenticationPage.emailField.sendKeys(TestDriver.tvnzProperties.getProperty("exampleEmail"));
        loginAndAuthenticationPage.passwordField.sendKeys(TestDriver.tvnzProperties.getProperty("examplePassword"));
        loginAndAuthenticationPage.loginButton.click();
        LandingPage landingPage = new LandingPage();
    }

    @And("^I navigate to a VOD episode$")
    public static void iNavigateToAVODEpisode() {
        // NOTE: User should already be logged in.
        TestDriver.automator.driver.navigate().to(TestDriver.tvnzProperties.getProperty("showPage"));
    }

    @And("^I click the (.*) button on the VOD episode video$")
    public void iPerformActionToTheEpisodeVideo(String action) {
        HelperMethods.sleep(5); //load page
        EpisodePage episodePage = new EpisodePage();
        String episodeShow = episodePage.showTitle.getText();
        if (action.equalsIgnoreCase("play")) {
            if (episodePage.loadingSpinner.exists(1)) {
                episodePage.loadingSpinner.waitForElementNotToBeDisplayed(60);
            }

            if (episodePage.bigPlayButton.isDisplayed(5)) {
                episodePage.bigPlayButton.click();
            }

            if (episodePage.adMessage.exists(10)) {
                episodePage.adMessage.waitForElementNotToExist(120);
            }


            // Wait for the ad to be over
            if (episodePage.adMessage.exists(5)) {
                episodePage.adMessage.waitForElementNotToExist(60);
            }

            episodePage.smallPlayButton.waitForElementNotToExist(20);
            HelperMethods.sleep(5); // Allow the video to play
        }
    }


    @Then("^I see the correct analytics call for a VOD play event$")
    public void iSeeTheCorrectAnalyticsCallForAVODPlayEvent() {
        HelperMethods.sleep(15); // wait for analytics to be available
        List<String> bodies = parseHarForAnalytics(TestDriver.tvnzProperties.getProperty("analyticsURL"), "play");
        if (bodies.size() < 1) {
            //try again to get the results
            HelperMethods.sleep(20);
            bodies = parseHarForAnalytics(TestDriver.tvnzProperties.getProperty("analyticsURL"), "play");
            if (bodies.size() < 1) {
                fail("The analytics call for a " + "Play" + " event was not found in the log.");

            }
        }

        JSONParser j = new JSONParser(bodies.get(0));
        assertEquals("video", j.findValue("properties.category"));
        assertEquals("play", j.findValue("event"));
        assertEquals("VOD", j.findValue("properties.videoStreamType"));
        assertFalse(j.findValue("properties.videoID").isEmpty());
        assertFalse(j.findValue("properties.smartWatch").isEmpty());
        assertFalse(j.findValue("properties.showID").isEmpty());
        assertFalse(j.findValue("properties.resume").isEmpty());
    }

    public static List<String> parseHarForAnalytics(String urlToFind, String dataName) {
        List<String> values = new ArrayList<>();
        for (HarEntry he : getHarLog().getLog().getEntries()) {
            if ((he.getRequest().getUrl().contains(urlToFind))) {
                if (he.getRequest().getMethod().contains("POST")) {
                    if (he.getRequest().getPostData().getText().contains("\"event\":\"" + dataName + "\"")) {
                        values.add(he.getRequest().getPostData().getText());
                    }
                }
            }
        }
        return values;
    }

    public static Har getHarLog() {
        return TestDriver.proxy.getHar();
    }

    @And("^I write out the HAR log$")
    public void iWriteOutTheHARLog() throws Throwable{
        File file = new File("HARlog_" + DateHelper.getCurrentDate().toString() + ".har");
        getHarLog().writeTo(file);
    }
}
